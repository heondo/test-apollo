const mongoose = require('mongoose')
mongoose.Promise = global.Promise
const uri = "mongodb://127.0.0.1:27017/"
// const {MDB_USER, MDB_PW} = process.env

// const uri = `mongodb+srv://${MDB_USER}:${MDB_PW}@cluster0-idizb.mongodb.net/test?retryWrites=true&w=majority`
const dbName = "atlas"

mongoose.connect(uri + dbName, {
  useNewUrlParser: true,
  useUnifiedTopology: true
}).catch(error => handleError(error))