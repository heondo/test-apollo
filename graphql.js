// 'use strict'
const { ApolloServer, gql } = require('apollo-server-lambda')
require('./config')

const {Student, Meeting} = require("./models")

const typeDefs = gql`
  type Query {
    students: [Student]
    student(id: ID!): Student
    meeting(id: ID!): Meeting
    meetings: [Meeting]
    # hello: String
  }

  type Mutation {
    addStudent(student: StudentInput): Student
    editStudent(id: ID!, student: StudentInput): Student
    addMeeting(meeting: MeetingInput): Meeting
  }

  type Student {
    _id: ID
    firstName: String
    lastName: String
    studentEmail: String
    studentPhone: String
    school: String
    graduationYear: Int
  }

  input StudentInput {
    _id: ID
    firstName: String
    lastName: String
    studentEmail: String
    studentPhone: String
    school: String
    graduationYear: Int
  }

  type Meeting {
    _id: ID
    name: String
    startTime: String
    duration: Int
    instructor: String
    location: String
    students: [Student]
    repeats: Boolean
    repeatOptions: RepeatOptions
  }

  input MeetingInput {
    name: String
    startTime: String
    duration: Int
    instructor: String
    location: String
    students: [ID]
    repeats: Boolean
    repeatOptions: RepeatOptionsInput
  }

  type RepeatOptions {
    numOccurences: Int
    repeatDays: [Int]
  }

  input RepeatOptionsInput {
    numOccurences: Int
    repeatDays: [Int]
  }
  
`;

// Provide resolver functions for your schema fields
const resolvers = {
  Query: {
    student: async (_, __, {id}) => {
      try {
        const student = await Student.findById(id).exec()
        return student
      } catch (err) {
        console.error(err)
      }
    },
    students: async () => {
      try {
        const students = await Student.find().exec()
        return students
      }
      catch (err) {
        console.error(err)
      }
    },
    meetings: async () => {
      try {
        const meetings = await Meeting.find().populate('students').exec()
        return meetings
      }
      catch (err) {
        console.error(err)
      }
    }
  },
  Mutation: {
    addStudent: async (_, {student}, context, info) => {
      try {
        const newStudent = new Student(student)
        await newStudent.save(err => {
          if (err) return handleError(err)
        })
        return newStudent
      } catch (err) {
        console.error(err)
      }
    },
    addMeeting: async (_, {meeting}, context, info) => {
      try {
        const newMeeting = new Meeting(meeting)
        await newMeeting.save(err => {
          if (err) return handleError(err)
        })
        await newMeeting.populate('students').execPopulate()
        // await newMeeting
        return newMeeting
      } catch (err) {
        console.error(err)
      }
    },
    // editStudent: () => {}
  }
}

const server = new ApolloServer({ 
  typeDefs,
  resolvers,
  context: ({ event, context }) => ({
    headers: event.headers,
    functionName: context.functionName,
    event,
    context,
  }),
 })

exports.graphqlHandler = server.createHandler({
  cors: {
    origin: true,
    credentials: true,
  },
})