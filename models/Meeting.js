// 'use strict'
const mongoose = require('mongoose')
const { Schema } = mongoose

const repeatSchema = new Schema({
  numOccurences: Number,
  repeatDays: Array
})

const meetingSchema = new Schema({
  id: Schema.Types.ObjectId,
  name: String,
  startTime: String,
  duration: Number,
  location: String,
  instructor: String,
  students: [{
    type: Schema.Types.ObjectId,
    ref: 'students'
  }],
  repeats: Boolean,
  repeatOptions: repeatSchema
})



const Meeting = mongoose.model('meetings', meetingSchema)

module.exports = {
  Meeting
}