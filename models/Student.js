// 'use strict'
const mongoose = require('mongoose')
const { Schema } = mongoose

const studentSchema = new Schema({
  id: Schema.Types.ObjectId,
  firstName: String,
  lastName: String,
  studentEmail: String,
  studentPhone: String,
  school: String,
  graduationYear: Number
})

const Student = mongoose.model('students', studentSchema)

module.exports = {
  Student
}