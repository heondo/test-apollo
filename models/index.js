const {Student} = require('./Student')
const {Meeting} = require('./Meeting')

module.exports = {
  Student,
  Meeting
}